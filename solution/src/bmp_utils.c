//
// Created by Andrey Vsiliev on 24.12.2021.
//
#include "../include/bmp_utils.h"

static uint64_t get_image_padding(uint64_t width);

//read functions
static enum read_status read_header(FILE *file, struct bmp_header *header);
static enum read_status read_pixels_bmp(FILE *file, struct image *img);

//write functions
static struct bmp_header create_header_bmp(struct image const *img);
static enum write_status write_pixels_bmp(FILE *out, struct image const *img);

enum read_status from_bmp(FILE *file, struct image *img) {
    if (!(file || img)) {
        return READ_NOTHING;
    }
    struct bmp_header header = {0};
    if (read_header(file, &header)) {
        return  READ_FAIL;
    }
    fseek(file, header.bOffBits, SEEK_SET);
    if (make_image(img, header.biWidth, header.biHeight) || read_pixels_bmp(file, img)) {
        return READ_FAIL;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    size_t count = 1;
    struct bmp_header header = create_header_bmp(img);
    if (fwrite(&header, sizeof(struct bmp_header), count, out) != 1) {
        return WRITE_ERROR;
    }
    fseek(out, header.bOffBits, SEEK_SET);
    if (write_pixels_bmp(out, img)) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

static enum write_status write_pixels_bmp(FILE *out, struct image const *img) {
    const size_t count = 1;
    const uint64_t padding = get_image_padding(img->width);
    uint32_t padding_vals[max_img_padding] = {0};
    for (size_t i = 0; i < img->height; ++i) {
        size_t res_d = fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), count, out);
        size_t res_p = fwrite(padding_vals, padding, count, out);
        if (res_d != count || res_p != count) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

static struct bmp_header create_header_bmp(struct image const *img) {
    struct bmp_header header = {0};

    size_t padding = get_image_padding(img->width);
    header.bfType = BF_TYPE;
    header.bfReserved = BF_RESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BI_SIZE;
    header.biSizeImage = img->height * (img->width * sizeof(struct pixel) + padding);
    header.bfileSize = header.bOffBits + header.biSizeImage;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_COUNT;
    header.biCompression = BI_COMPRESSION;
    header.biXPelsPerMeter = PIXELS_PER_METER;
    header.biYPelsPerMeter = PIXELS_PER_METER;
    header.biClrUsed = BI_CLR_USED;
    header.biClrImportant = BI_CLR_IMPORTANT;

    return header;
}


static enum read_status read_pixels_bmp(FILE *file, struct image *img) {
    size_t count = 1;
    size_t size = (img->width) * sizeof(struct pixel);
    size_t padding = get_image_padding(img->width);
    for (size_t i = 0; i < img->height; i++) { //read line by line
        size_t res_d =fread(img->data + i * (img->width), size, count, file);
        size_t res_p =fseek(file, padding, SEEK_CUR);
        if (res_d !=count || res_p!=0) {
            return READ_FAIL;
        }
    }
    return READ_OK;
}

static uint64_t get_image_padding(uint64_t width) {
    const uint64_t r = (width * sizeof(struct pixel))%4;
    return r != 0 ? (4 - r): 0;
}

static enum read_status read_header(FILE *file_in, struct bmp_header *header) {
    size_t count = 1;
    size_t result = fread(header, sizeof(struct bmp_header), count, file_in);
    if (result != count) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}
