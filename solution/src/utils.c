#include "../include/bmp_utils.h"
#include "../include/transform.h"

_Noreturn void err( const char* msg) {
    fprintf(stderr,"%s", msg);
    exit(1);
}

bool rotate_bmp(FILE *input_file,FILE *output_file){
    struct image original_image;
    struct image transformed_image;

    switch (from_bmp(input_file, &original_image)) {
        case READ_NOTHING:
            err("Файли или картинка не найдены\n");
            break;
        case READ_FAIL:
            err("Формат картинки должен быть BMP или файл поврежден\n");
            break;
        default:
            break;
    }

    fprintf(stdout, "Файл сконвертировался\n");


    transformed_image = rotate_90_degrees(&original_image);

    if(to_bmp(output_file,&transformed_image)){
        err("Не получилось сконвертировать перевернуютую картинку в .bmp\n");
    }

    fprintf(stdout, "Вы получили повернутую на 90 градусов bmp картинку!\n");


    image_destroy( &original_image);
    image_destroy( &transformed_image);
    return true;
}

