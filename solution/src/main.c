#include "../include/utils.h"


int main(int argc, char **argv) {
    if (argc != 3) {
        err("Верное кол-во аргументов 3, правильный формат ./executable_file input.bmp output.bmp\n");
    }

    //Открываем картинки
    FILE *in_file= NULL, *out_file= NULL;
    in_file= fopen(argv[1], "rb");
    out_file= fopen(argv[2], "wb");

    if (in_file== NULL) {
        err("Ошибка при открытии файла\n");
    } else if (out_file== NULL) {
        err("Ошибка при создании файла\n");
    }

    //Поворачиваем картинку
    rotate_bmp(in_file,out_file);

    //Закрываем картинки
    fclose(in_file);
    fclose(out_file);

    return 0;
}
