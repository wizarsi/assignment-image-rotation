//
// Created by Andrey Vsiliev on 24.12.2021.
//

#include "../include/image.h"

bool make_image( struct image* image,uint64_t width,uint64_t height){
    image->height = height;
    image->width = width;
    image->data = malloc(sizeof(struct pixel) * width * height);
    return image->data ==NULL;
}

void image_destroy( struct image* image ){
    free(image->data);
}
