//
// Created by Andrey Vsiliev on 25.12.2021.
//

#include "../include/transform.h"
#include "../include/image.h"

struct image rotate_90_degrees(struct image *in_img) {
    struct image out_img;
    uint64_t width = in_img->width, height = in_img->height;
    make_image(&out_img, height, width);
    for (uint64_t i = 0; i < height; i++) {
        for (uint64_t j = 0; j < width; j++) {
            out_img.data[(height) * (j + 1) - i - 1] = in_img->data[(i * width) + j];
        }
    }
    return out_img;
}

