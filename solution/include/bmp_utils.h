//
// Created by Andrey Vsiliev on 24.12.2021.
//

#include "./image.h"
#include <stdio.h>
#define max_img_padding 3
#define BF_TYPE 19778
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_COUNT  24
#define BI_COMPRESSION 0
#define PIXELS_PER_METER 0
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0



#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)


/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_NOTHING,
    READ_FAIL,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

enum read_status from_bmp(FILE *file, struct image *img);

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_NOTHING,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );
