//
// Created by Andrey Vsiliev on 24.12.2021.

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

_Noreturn void err( const char* msg);

bool rotate_bmp(FILE *input_file,FILE *output_file);
